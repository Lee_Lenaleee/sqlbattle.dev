import dev.sqlbattle.backend.model.Battle;
import dev.sqlbattle.backend.sqlite.SQLiteInitializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

public class SQLiteInitializerTest {

	public Battle createFirstBattle() {
		return new Battle(
				1,
				UUID.fromString("650f464f-c81a-4050-a2bf-4daac8139873"),
				"Basic selection",
				"Selecting data from a database is the first step to succes.",
				"data:image/png;base64,",
				"CREATE TABLE users(id, name, salary);INSERT INTO users VALUES(2304, 'Tim', 400);INSERT INTO users VALUES(1996, 'Faab', 250);INSERT INTO users VALUES(8181, 'Jesper', 225);",
				"[{\"id\":2304,\"name\":\"Tim\",\"salary\":400}]",
				"CREATE TABLE users(id, name, salary);INSERT INTO users VALUES(1, 'Aab', 500);INSERT INTO users VALUES(0, 'Noot', 350);INSERT INTO users VALUES(3, 'Mies', 825);",
				"[{\"id\":3,\"name\":\"Mies\",\"salary\":825}]",
				UUID.fromString("650f464f-c81a-4050-a2bf-4daac8139873"),
				20000,
				UUID.fromString("00000000-0000-0000-0000-000000000000"),
				3000,
				null,
				0
		);
	}

	@BeforeAll
	public static void setup() {
		new SQLiteInitializer().init();
	}

	@AfterEach
	public void cleanup() throws Exception {
		// After
		assertThat("SQL files", Files.list(Paths.get("db")).count(), is(0L));
	}

	@Test
	public void test_battle_short_user_sql() {
		// Arrange
		Battle battle = createFirstBattle();
		String sql = "select*from users order by salary desc limit 1";
		double expectedScore = 2173.91;

		// Act
		double[] score = { 0 };
		SQLiteInitializer.getScoreWith(battle, sql, (gson, scoreIn) -> score[0] = scoreIn);

		// Assert
		assertThat("SQLBattle score", score[0], closeTo(expectedScore, 1));
	}

	@Test
	public void test_battle_short_v2_user_sql() {
		// Arrange
		Battle battle = createFirstBattle();
		String sql = "SELECT id,name,max(salary)as salary FROM users";
		double expectedScore = 2173.91;

		// Act
		double[] score = { 0 };
		SQLiteInitializer.getScoreWith(battle, sql, (gson, scoreIn) -> score[0] = scoreIn);

		// Assert
		assertThat("SQLBattle score", score[0], closeTo(expectedScore, 1));
	}

	@Test
	public void test_battle_stupid_user_sql() {
		// Arrange
		Battle battle = createFirstBattle();
		String sql = "SELECT u.id,u.name,u.salary\n" +
				"FROM users u\n" +
				"WHERE u.salary=400 OR u.salary=825";
		double expectedScore = 1333.33;

		// Act
		double[] score = { 0 };
		SQLiteInitializer.getScoreWith(battle, sql, (gson, scoreIn) -> score[0] = scoreIn);

		// Assert
		assertThat("SQLBattle score", score[0], closeTo(expectedScore, 1));
	}
}
