package dev.sqlbattle.backend.database.primarykey;

import java.util.Date;
import java.util.UUID;

public class DoubleUUIDWithTimeKey implements Key {

	private final UUID keyA;
	private final UUID keyB;
	private final Date keyC;

	public DoubleUUIDWithTimeKey(UUID keyA, UUID keyB, Date keyC) {
		this.keyA = keyA;
		this.keyB = keyB;
		this.keyC = keyC;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	public Date getKeyC() {
		return keyC;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB(), getKeyC() };
	}
}
