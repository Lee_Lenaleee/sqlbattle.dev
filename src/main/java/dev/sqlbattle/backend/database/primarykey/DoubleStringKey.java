package dev.sqlbattle.backend.database.primarykey;

public class DoubleStringKey implements Key {

	private final String keyA;
	private final String keyB;

	public DoubleStringKey(String keyA, String keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public String getKeyA() {
		return keyA;
	}

	public String getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
