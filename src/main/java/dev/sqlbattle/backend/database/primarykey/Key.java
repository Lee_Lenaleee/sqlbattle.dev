package dev.sqlbattle.backend.database.primarykey;

public interface Key {

	Object[] getKeys();

}
