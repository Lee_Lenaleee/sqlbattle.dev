package dev.sqlbattle.backend.database.primarykey;

import java.util.Date;

public class DateKey implements Key {

	private final Date key;

	public DateKey(Date key) {
		this.key = key;
	}

	public Date getKey() {
		return key;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKey() };
	}
}
