package dev.sqlbattle.backend.database;

import java.util.Arrays;
import java.util.stream.IntStream;

public class SQLBuilder {

	public static String create(String tableName, String... columns) {
		return String.format("INSERT INTO %s (%s) VALUES (%s);",
				tableName,
				String.join(", ", Arrays.stream(columns).map(SQLBuilder::safeColumnName).toArray(String[]::new)),
				String.join(", ", toQuestionMarks(columns))
		);
	}

	public static String createIgnore(String tableName, String... columns) {
		return String.format("INSERT IGNORE INTO %s (%s) VALUES (%s);",
				tableName,
				String.join(", ", Arrays.stream(columns).map(SQLBuilder::safeColumnName).toArray(String[]::new)),
				String.join(", ", toQuestionMarks(columns))
		);
	}

	public static String createMultiple(String tableName, int times, String... columns) {
		String questionMarks = toQuestionMarks(columns);
		String values = String.join(",", IntStream.range(0, times).mapToObj(i -> "(" + questionMarks + ")").toArray(String[]::new));
		return String.format("INSERT INTO %s (%s) VALUES %s;",
				tableName,
				String.join(", ", Arrays.stream(columns).map(SQLBuilder::safeColumnName).toArray(String[]::new)),
				values
		);
	}

	public static String combineWhere(String query, String where) {
		if (!query.contains("GROUP BY")) {
			return query + where;
		}
		return query.substring(0, query.indexOf("GROUP BY")) + " " + where + " " + query.substring(query.indexOf("GROUP BY"));
	}

	public static String readOne(String tableName, String... columns) {
		return readX(tableName, 1, columns);
	}

	public static String readOneWithOrder(String tableName, String order, String... columns) {
		return readXWithOrder(tableName, 1, order, columns);
	}

	public static String readX(String tableName, int limit, String... columns) {
		return readXWithOrder(tableName, limit, "", columns);
	}

	public static String readXWithOrder(String tableName, int limit, String order, String... columns) {
		return readAll(tableName, columns) + order + (limit > 0 ? " LIMIT " + limit : "");
	}

	public static String readXAt(String tableName, int limit, int offset, String... columns) {
		return readXAtWithOrder(tableName, limit, offset, "", columns);
	}

	public static String readXAtWithOrder(String tableName, int limit, int offset, String orderBy, String... columns) {
		return readAll(tableName, columns) + orderBy + (limit > 0 ? " LIMIT " + limit : "") + (offset > 0 ? " OFFSET " + offset : "");
	}

	public static String readAll(String tableName, String... columns) {
		return String.format("SELECT %s FROM %s",
				columns.length == 0 ? "*" : String.join(", ", columns),
				tableName
		);
	}

	public static String readAllWhereOrderBy(String tableName, String where, String order, String... columns) {
		return String.format("SELECT %s FROM %s %s ORDER BY %s",
				columns.length == 0 ? "*" : String.join(", ", columns),
				tableName,
				where,
				order
		);
	}

	public static String readXWhereOrderBy(String tableName, String where, String order, int limit, String... columns) {
		return String.format("SELECT %s FROM %s %s ORDER BY %s %s",
				columns.length == 0 ? "*" : String.join(", ", columns),
				tableName,
				where,
				order,
				(limit > 0 ? " LIMIT " + limit : "")
		);
	}

	public static String readAllOrderBy(String tableName, String order, String... columns) {
		return String.format("SELECT %s FROM %s ORDER BY %s",
				columns.length == 0 ? "*" : String.join(", ", columns),
				tableName,
				order
		);
	}

	public static String count(String tableName) {
		return String.format("SELECT COUNT(*) as count FROM %s", tableName);
	}

	public static String countWhere(String tableName, String where) {
		return String.format("SELECT COUNT(*) as count FROM %s %s", tableName, where);
	}

	public static String update(String tableName, String... columns) {
		return String.format("UPDATE %s SET %s",
				tableName,
				String.join(", ", Arrays.stream(columns).map(column -> safeColumnName(column) + "=?").toArray(String[]::new))
		);
	}

	public static String delete(String tableName) {
		return "DELETE FROM " + tableName;
	}

	public static String whereUnique(String... columns) {
		return " WHERE " + String.join(" AND ", Arrays.stream(columns).map(column -> safeColumnName(column) + "=?").toArray(String[]::new));
	}

	private static String safeColumnName(String column) {
		return String.join(".",
				Arrays.stream(column.split("\\."))
						.map(columnProperty -> "`" + columnProperty + "`")
						.toArray(String[]::new)
		);
	}

	private static String toQuestionMarks(String... columns) {
		return String.join(", ", Arrays.stream(columns).map(column -> "?").toArray(String[]::new));
	}
}
