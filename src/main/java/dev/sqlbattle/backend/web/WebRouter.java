package dev.sqlbattle.backend.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.general.*;
import dev.sqlbattle.backend.web.routes.*;
import org.apache.commons.lang3.exception.ExceptionUtils;
import spark.Request;
import spark.Response;
import spark.RouteImpl;
import spark.Service;
import spark.route.HttpMethod;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebRouter implements Initializer {

	private final List<Route> routerMap = Arrays.asList(
			new LandingRoute(),
			new SubmitRoute(),
			new BattleRoute(),
			new UserRoute(),
			new TestRoute()
	);

	public static final List<String> allowedOrigins = Arrays.asList(
			"http://localhost:4200",
			"http://localhost:8080",
			"http://192.168.1.105:4200",
			"https://sqlbattle.dev",
			"https://alpha.sqlbattle.dev",
			"https://www.sqlbattle.dev",
			"https://beta.sqlbattle.dev",
			"https://alpha.sqlbattle.dev"
	);

	public static final Gson GSON = new GsonBuilder()
			.setPrettyPrinting()
			.serializeNulls()
			.registerTypeAdapter(Throwable.class, new ThrowableJsonAdapter())
			.setDateFormat("dd-MM-yyyy HH:mm:ssZ")
			.addSerializationExclusionStrategy(new ExclusionStrategy(KeySet.class))
			.create();

	private Service service;
	private final String serverTimeHeader = "server-time";

	@Override
	public void init() {
		service = Service.ignite();
		service.staticFiles.location("public");
		service.ipAddress(Settings.SETTINGS.getString("server.host"));
		service.port(Settings.SETTINGS.getInt("server.port"));
		service.threadPool(
				Settings.SETTINGS.getInt("server.maxThreads"),
				Settings.SETTINGS.getInt("server.minThreads"),
				Settings.SETTINGS.getInt("server.timeoutThreads")
		);
		registerCors();
		registerRoutes();
	}

	private void registerRoutes() {
		Logger.verbose("Registering routes");
		Map<String, String> registeredRoutes = new HashMap<>();
		routerMap.forEach(route -> {
			Logger.verbose(" " + route.getClass().getName() + ":");
			route.getRequests().asMap().forEach((routeString, routeMethods) -> {
				routeMethods.forEach(routeMethod -> {
					Logger.verbose(" + " + routeMethod.toString().toUpperCase() + "  " + routeString);

					String key = routeMethod.toString() + "~" + routeString;
					if (registeredRoutes.containsKey(key)) {
						Logger.error("Route conflict "
								+ registeredRoutes.get(key)
								+ " <-> "
								+ route.getClass().getName());
						Logger.error(" conflict with route " + "(" + routeMethod.toString().toUpperCase() + ") " + routeString);
						throw new RuntimeException("Route " + key + " at "
								+ route.getClass().getName()
								+ "("
								+ route.getClass().getSimpleName() + ".java"
								+ ") has a duplicate");
					}
					registeredRoutes.put(key, route.getClass().getName());

					service.addRoute(routeMethod, RouteImpl.create(routeString,
							(req, res) -> handleRouteRequest(req, res, route)
					));
				});
			});
		});
		Logger.verbose("");
	}

	private String handleRouteRequest(Request request, Response response, Route route) {
		Object responseObject = SafeTry.execute(
				() -> route.call(HttpMethod.get(request.requestMethod()), request, response),
				ex -> {
					String[] stacktrace = ExceptionUtils.getStackTrace(ex).replaceAll("[\t\r]", "").split("\n");
					return ArrayHelper.splitArrayTill(stacktrace, stack -> stack.contains("at spark"));
				}
		);
		response.header("Content-Type", "application/json");
		return GSON.toJson(responseObject);
	}

	private void registerCors() {
		Logger.verbose("Allowing all CORS");
		this.service.options("/*", (req, res) -> "\"OK\"");

		this.service.before((req, res) -> {
			String origin = req.headers("origin");
			if (origin == null) {
				return;
			}
			if (!allowedOrigins.contains(origin)) {
				Logger.verbose("NOT ALLOWED ORIGIN FROM " + origin + " - " + Route.getClientIp(req));
				return;
			}
			res.header("Access-Control-Allow-Origin", origin);

			String method = req.headers("Access-Control-Request-Method");
			if (method == null) {
				method = "GET, POST, PUT, DELETE, OPTIONS";
			}
			res.header("Access-Control-Allow-Methods", method);
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Max-Age", "3600");
			res.header("Access-Control-Allow-Headers", "*");
		});
	}

}
