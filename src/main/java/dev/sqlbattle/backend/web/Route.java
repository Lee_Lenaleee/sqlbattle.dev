package dev.sqlbattle.backend.web;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.model.Player;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.util.Optional;

public interface Route {

	MultiValuedMap<String, HttpMethod> getRequests();

	Object call(HttpMethod method, Request request, Response response);

	static String getClientIp(Request request) {
		String[] headers = new String[] {
				"HTTP_CLIENT_IP",
				"HTTP_X_FORWARDED_FOR",
				"HTTP_X_FORWARDED",
				"HTTP_FORWARDED_FOR",
				"HTTP_FORWARDED",
				"REMOTE_ADDR"
		};

		for (String header : headers) {
			String headerValue = request.headers(header);
			if (headerValue != null && !headerValue.trim().isEmpty()) {
				return headerValue;
			}
		}
		return "UNKNOWN";
	}

	default Optional<Player> getLoggedIn(Request request) {
		return Database.getConnection(connection -> {
			return Authentication.login(connection, request);
		});
	}

	default Player requirePlayer(Request request) {
		return getLoggedIn(request).orElseThrow(() -> new RuntimeException("Not logged in"));
	}

	static String headerOrQueryOrParamOrBody(Request request, String param) {
		if (request.headers().contains(param)) {
			return request.headers(param);
		}
		if (request.queryParams().contains(param)) {
			return request.queryParams(param);
		}
		if (request.params().containsKey(":" + param)) {
			return request.params(":" + param);
		}
		String body = request.body();
		if (body.trim().startsWith("{")) {
			JsonObject bodyJson = new JsonParser().parse(body).getAsJsonObject();
			if (bodyJson.has(param)) {
				return bodyJson.get(param).getAsString();
			}
		}
		return null;
	}

	class RouteRequestBuilder {

		MultiValuedMap<String, HttpMethod> requests = new ArrayListValuedHashMap<>();

		public RouteRequestBuilder add(HttpMethod method, String route) {
			requests.put(route, method);
			return this;
		}

		public MultiValuedMap<String, HttpMethod> build() {
			return requests;
		}
	}
}
