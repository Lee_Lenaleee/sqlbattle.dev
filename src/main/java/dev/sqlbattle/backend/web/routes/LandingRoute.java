package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LandingRoute implements Route {

	private final List<Long> visitors = new LinkedList<>();

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		return new RouteRequestBuilder()
				.add(HttpMethod.get, "/")
				.add(HttpMethod.get, "/index.php")
				.add(HttpMethod.get, "/index.asp")
				.add(HttpMethod.get, "/index.htm")
				.add(HttpMethod.get, "/index.pl")
				.add(HttpMethod.get, "/index.cgi")
				.add(HttpMethod.get, "/index.xhtml")
				.add(HttpMethod.get, "/index")
				.add(HttpMethod.get, "/usage")
				.build();
	}

	@Override
	public Object call(HttpMethod method, Request request, Response response) {
		if (request.uri().equals("/usage")) {
			synchronized (visitors) {
				long now = System.currentTimeMillis();
				visitors.add(now);
				Iterator<Long> iterator = visitors.iterator();
				while (iterator.hasNext()) {
					Long visitor = iterator.next();
					if (visitor < now - (1000 * 60 * 60)) {
						iterator.remove();
					} else {
						break;
					}
				}
			}
			return visitors.size();
		}
		response.header("location", "index.html");
		response.status(301);
		return "You get redirected to, if not please follow the url to https://SQLBattle.dev/index.html";
	}
}
