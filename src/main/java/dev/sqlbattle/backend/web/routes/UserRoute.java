package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.dao.PlayerDAO;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.database.primarykey.StringKey;
import dev.sqlbattle.backend.general.RegexStringConsumer;
import dev.sqlbattle.backend.general.security.HashSequence;
import dev.sqlbattle.backend.model.Player;
import dev.sqlbattle.backend.model.PlayerWithLogin;
import dev.sqlbattle.backend.model.PlayerWithScore;
import dev.sqlbattle.backend.web.Authentication;
import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dev.sqlbattle.backend.web.Route.headerOrQueryOrParamOrBody;

public class UserRoute implements Route {

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		return new RouteRequestBuilder()
				.add(HttpMethod.get, "/top")
				.add(HttpMethod.get, "/users")
				.add(HttpMethod.get, "/user")
				.add(HttpMethod.get, "/user/:id")
				.add(HttpMethod.get, "/user/register")
				.add(HttpMethod.post, "/user/register")
				.add(HttpMethod.get, "/user/login")
				.add(HttpMethod.post, "/user/login")
				.add(HttpMethod.get, "/user/update")
				.add(HttpMethod.post, "/user/update")
				.build();
	}

	@Override
	public Object call(HttpMethod method, Request request, Response response) {
		switch (request.uri()) {
			case "/user":
				return getLoggedInPlayer(request);
			case "/users":
			case "/top":
				return getUsers();
			case "/user/register":
				return register(request);
			case "/user/login":
				return login(request);
			case "/user/update":
				return update(request);
		}
		if (request.uri().startsWith("/user/")) {
			return findUser(request);
		}
		throw new RuntimeException("Method not implemented exception");
	}

	private Player getLoggedInPlayer(Request request) {
		return getLoggedIn(request).orElse(null);
	}

	private List<PlayerWithScore> getUsers() {
		return Database.getConnection(connection -> {
			return PlayerDAO.getInstance(PlayerDAO.class).getPlayersSortedByScore(connection, 100);
		});
	}

	private Player findUser(Request request) {
		String name = request.params("id");
		return Database.getConnection(connection -> {
			return PlayerDAO.getInstance(PlayerDAO.class)
					.findFirstWithKey(connection, new KeySet(new StringKey(name), "username"))
					.orElse(null);
		});
	}

	private String validateUsername(Request request) {
		String username = headerOrQueryOrParamOrBody(request, "username");
		if (username == null || username.trim().isEmpty() || !username.matches("^[A-Za-z0-9]{1,128}$")) {
			throw new RuntimeException("Username is missing or invalid");
		}
		return username;
	}

	private String validatePassword(Request request) {
		String password = headerOrQueryOrParamOrBody(request, "password");
		if (password == null || password.trim().length() < 4) {
			throw new RuntimeException("Password is missing or invalid");
		}
		return password;
	}

	private Player login(Request request) {
		String username = validateUsername(request);
		String password = validatePassword(request);

		return Database.getConnection(connection -> {
			Optional<PlayerWithLogin> optionalPlayer = PlayerDAO.getInstance(PlayerDAO.class)
					.findLoginUserByUsername(connection, username);
			if (!optionalPlayer.isPresent()) {
				throw new RuntimeException("Username not found");
			}

			return Authentication
					.checkPassword(optionalPlayer.get(), password)
					.orElseThrow(() -> new RuntimeException("Password incorrect"));
		});
	}

	private Player register(Request request) {
		String username = validateUsername(request);
		String password = validatePassword(request);

		return Database.getConnection(connection -> {
			if (PlayerDAO.getInstance(PlayerDAO.class)
					.findUserByUsername(connection, username).isPresent()) {
				throw new RuntimeException("Username is already in use");
			}

			UUID uuid = UUID.randomUUID();
			String passwordHash = HashSequence.hash(uuid, password);
			Player player = new PlayerWithLogin(uuid, username, passwordHash);
			if (PlayerDAO.getInstance(PlayerDAO.class).create(connection, player)) {
				return player;
			}
			throw new RuntimeException("Server couldn't save your profile");
		});
	}

	private Player update(Request request) {
		final Player player = requirePlayer(request);

		Map<String, RegexStringConsumer> features = new HashMap<>();
		features.put("image", new RegexStringConsumer("data:image\\/[^;]+;base64[^\"]+", image -> player.image = image));
		features.put("jobTitle", new RegexStringConsumer(".*", jobTitle -> player.jobTitle = jobTitle));
		features.put("website", new RegexStringConsumer("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)", website -> player.website = website));
		features.put("git", new RegexStringConsumer("(https?:\\/\\/)?([^/]+)\\/([^/\\n]+)", git -> {
			if (git == null) {
				player.gitServer = null;
				player.git = null;
				return;
			}
			@SuppressWarnings("RegExpRedundantEscape")
			Matcher matcher = Pattern.compile("(?:https?:\\/\\/)?([^/]+)\\/([^/\\n]+)").matcher(git);
			if (!matcher.matches()) {
				throw new RuntimeException("No valid git username url");
			}
			String gitServer = matcher.group(1);
			String gitUsername = matcher.group(2);
			if (gitServer.contains("lab")) {
				player.gitServer = "lab";
			} else if (gitServer.contains("hub")) {
				player.gitServer = "hub";
			} else if (gitServer.contains("bucket")) {
				player.gitServer = "bitbucket";
			} else {
				throw new RuntimeException("no support yet for git server " + gitServer);
			}
			player.git = gitUsername;
		}));
		features.put("twitter", new RegexStringConsumer("^(\\w){1,15}$", twitter -> player.twitter = twitter));
		features.put("hyven", new RegexStringConsumer("^[\\p{N}\\p{L}_-]{3,32}$", hyven -> player.hyven = hyven));
		features.put("codepen", new RegexStringConsumer("^(\\w){1,75}$", codepen -> player.codepen = codepen));

		features.forEach((feature, consumer) -> {
			String param = headerOrQueryOrParamOrBody(request, feature);
			// if not given, ignore it
			if (param == null) {
				return;
			}
			// if given but empty, remove it form profile
			if (param.trim().isEmpty()) {
				param = null;
			}
			if (param == null || param.matches(consumer.regex)) {
				consumer.action.accept(param);
			}
		});

		Database.getConnection(connection -> {
			if (!PlayerDAO.getInstance(PlayerDAO.class).save(connection, player)) {
				throw new RuntimeException("Something while saving went wrong");
			}
		});
		return player;
	}
}
