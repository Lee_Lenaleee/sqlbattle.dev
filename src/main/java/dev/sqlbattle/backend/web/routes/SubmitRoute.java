package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.dao.BattleDAO;
import dev.sqlbattle.backend.dao.BattleSubmitDAO;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.database.primarykey.IntKey;
import dev.sqlbattle.backend.database.primarykey.IntUUIDStringKey;
import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.general.SafeTry;
import dev.sqlbattle.backend.model.Battle;
import dev.sqlbattle.backend.model.BattleSubmit;
import dev.sqlbattle.backend.model.Player;
import dev.sqlbattle.backend.sqlite.SQLiteInitializer;
import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.util.Optional;

public class SubmitRoute implements Route {

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		return new RouteRequestBuilder()
				.add(HttpMethod.put, "/submit")
				.add(HttpMethod.put, "/submit/:id")
				.build();
	}

	@Override
	public BattleSubmit call(HttpMethod method, Request request, Response response) {
		Player player = requirePlayer(request);

		String battleIdString = Route.headerOrQueryOrParamOrBody(request, "id");
		if (battleIdString == null) {
			throw new RuntimeException("Battle id not given");
		}
		int battleId = SafeTry.execute(() -> Integer.parseInt(battleIdString));
		String sql = Route.headerOrQueryOrParamOrBody(request, "sql");
		if (sql == null || sql.trim().isEmpty()) {
			throw new RuntimeException("SQL not given");
		}

		BattleSubmit battleSubmit = Database.getConnection(connection -> {
			Optional<BattleSubmit> sameSubmit = BattleSubmitDAO.getInstance(BattleSubmitDAO.class)
					.findFirstWithKey(connection, new KeySet(new IntUUIDStringKey(battleId, player.getUUID(), sql), "battle_id", "player_id", "sql"));
			if (sameSubmit.isPresent()) {
				throw new RuntimeException("Same solution as before");
			}

			Battle battle = BattleDAO.getInstance(BattleDAO.class)
					.findFirstWithKey(connection, new KeySet(new IntKey(battleId), "id"))
					.orElseThrow(() -> new RuntimeException("Battle not found"));
			BattleSubmit submit = fromRequest(battle, player, sql);

			if (battle.checkInsertScore(player.getUUID(), submit.score)) {
				if (!BattleDAO.getInstance(BattleDAO.class)
						.save(connection, battle)) {
					throw new RuntimeException("Failed to safe highscore");
				}
			}

			if (!BattleSubmitDAO.getInstance(BattleSubmitDAO.class)
					.create(connection, submit)) {
				throw new RuntimeException("Failed to safe result");
			}
			return submit;
		});
		return battleSubmit;
	}

	private BattleSubmit fromRequest(Battle battle, Player player, String sql) {
		String[] resultTable = { null };
		double[] score = { 0 };
		SQLiteInitializer.getScoreWith(battle, sql, (preview, outputScore) -> {
			resultTable[0] = preview;
			score[0] = outputScore;
		});

		return new BattleSubmit(null, battle.getId(), player.getUUID(), sql, resultTable[0], score[0]);
	}
}
