package dev.sqlbattle.backend.model;

import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.database.primarykey.UUIDKey;

import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class Player implements Model<Player> {

	public KeySet keySet;
	public String username;
	public transient String password;
	public String image;
	@DatabaseName("job_title")
	public String jobTitle;
	public String website;
	@DatabaseName("git_server")
	public String gitServer;
	public String git;
	public String twitter;
	public String hyven;
	public String codepen;

	public Player(UUID key, String username, String password) {
		this(key, username, password, null, null, null, null, null, null, null, null);
	}

	public Player(UUID key, String username, String password, String image, String jobTitle, String website, String gitServer, String git, String twitter, String hyven, String codepen) {
		this.keySet = new KeySet(new UUIDKey(key), "uuid");
		this.username = username;
		this.password = password;
		this.image = image;
		this.jobTitle = jobTitle;
		this.website = website;
		this.gitServer = gitServer;
		this.git = git;
		this.twitter = twitter;
		this.hyven = hyven;
		this.codepen = codepen;
	}

	@Override
	public KeySet getPrimaryKey() {
		return keySet;
	}

	public UUID getUUID() {
		return keySet.getKeyAs(UUIDKey.class).getKey();
	}

	@Override
	public String toString() {
		return getUUID().toString();
	}
}
