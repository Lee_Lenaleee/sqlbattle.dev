package dev.sqlbattle.backend.model;

import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.general.ArrayHelper;
import dev.sqlbattle.backend.general.Logger;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.UUID;

public interface Model<M extends Model<M>> {

	KeySet getPrimaryKey();

	default String[] getColumns() {
		Field[] variables = getClass().getFields();

		return ArrayHelper.combine(
				Arrays.stream(variables)
						.filter(variable -> !KeySet.class.isAssignableFrom(variable.getType()))
						.filter(variable -> variable.getAnnotation(DatabaseIgnore.class) == null)
						.map(variable -> {
							DatabaseName name = variable.getAnnotation(DatabaseName.class);
							if (name != null) {
								return name.value();
							}
							return variable.getName();
						}).toArray(String[]::new),
				getPrimaryKey().getKeyColumns(),
				String[]::new
		);
	}

	default Object[] getValues() {
		Field[] variables = getClass().getFields();

		return ArrayHelper.combine(
				Arrays.stream(variables)
						.filter(variable -> !KeySet.class.isAssignableFrom(variable.getType()))
						.filter(variable -> variable.getAnnotation(DatabaseIgnore.class) == null)
						.map(variable -> {
							try {
								Object value = variable.get(this);
								if (variable.getAnnotation(DatabaseToString.class) != null || value instanceof UUID) {
									value = value != null ? value.toString() : null;
								}
								return value;
							} catch (Throwable throwable) {
								Logger.warn(throwable);
							}
							return null;
						}).toArray(Object[]::new),
				getPrimaryKey().getKeyValues(),
				Object[]::new
		);
	}

	default boolean equals(M other) {
		if (other == null) {
			return false;
		}
		return this.getPrimaryKey().equals(other.getPrimaryKey());
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	@interface DatabaseIgnore {
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	@interface DatabaseToString {
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	@interface DatabaseName {
		String value();
	}
}
