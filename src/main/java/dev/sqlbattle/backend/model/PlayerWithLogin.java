package dev.sqlbattle.backend.model;

import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class PlayerWithLogin extends Player {

	@DatabaseIgnore
	public UUID id;

	public PlayerWithLogin(UUID key, String username, String password) {
		this(key, username, password, null, null, null, null, null, null, null, null);
	}

	public PlayerWithLogin(UUID key, String username, String password, String image, String jobTitle, String website, String gitServer, String git, String twitter, String hyven, String codepen) {
		super(key, username, password, image, jobTitle, website, gitServer, git, twitter, hyven, codepen);
		this.id = key;
	}
}
