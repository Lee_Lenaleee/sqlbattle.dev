package dev.sqlbattle.backend.model;

import dev.sqlbattle.backend.database.primarykey.IntKey;
import dev.sqlbattle.backend.database.primarykey.KeySet;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class Battle implements Model<Battle> {

	public KeySet keySet;
	@DatabaseIgnore
	public int id;
	public String name;
	public String desc;
	public String image;
	public String dataset;
	@DatabaseName("preview_result")
	public String previewResult;
	@DatabaseName("test_dataset")
	public String testDataset;
	@DatabaseName("test_result")
	public String testResult;

	public transient UUID submitted_by;
	@DatabaseIgnore
	public Player submittedBy;

	@DatabaseName("cache_win_1")
	public transient UUID cacheWinFirstId;
	@DatabaseIgnore
	public Player cacheWinFirst;
	@DatabaseName("cache_win_1_score")
	public double cacheWinFirstScore;

	@DatabaseName("cache_win_2")
	public transient UUID cacheWinSecondId;
	@DatabaseIgnore
	public Player cacheWinSecond;
	@DatabaseName("cache_win_2_score")
	public double cacheWinSecondScore;

	@DatabaseName("cache_win_3")
	public transient UUID cacheWinThirdId;
	@DatabaseIgnore
	public Player cacheWinThird;
	@DatabaseName("cache_win_3_score")
	public double cacheWinThirdScore;

	public Battle(int key, UUID submitted_by, String name, String desc, String image, String dataset, String previewResult, String testDataset, String testResult, UUID cacheWinFirstId, double cacheWinFirstScore, UUID cacheWinSecondId, double cacheWinSecondScore, UUID cache_win_3, double cacheWinThirdScore) {
		this.keySet = new KeySet(new IntKey(key), "id");
		this.id = key;
		this.name = name;
		this.desc = desc;
		this.submitted_by = submitted_by;
		this.image = image;
		this.dataset = dataset;
		this.previewResult = previewResult;
		this.testDataset = testDataset;
		this.testResult = testResult;
		this.cacheWinFirstId = cacheWinFirstId;
		this.cacheWinFirstScore = cacheWinFirstScore;
		this.cacheWinSecondId = cacheWinSecondId;
		this.cacheWinSecondScore = cacheWinSecondScore;
		this.cacheWinThirdId = cache_win_3;
		this.cacheWinThirdScore = cacheWinThirdScore;
	}

	public void setSubmittedBy(Player submittedBy) {
		this.submittedBy = submittedBy;
	}

	public void setCacheWinFirst(Player cacheWinFirst) {
		this.cacheWinFirst = cacheWinFirst;
	}

	public void setCacheWinSecond(Player cacheWinSecond) {
		this.cacheWinSecond = cacheWinSecond;
	}

	public void setCacheWinThird(Player cacheWinThird) {
		this.cacheWinThird = cacheWinThird;
	}

	public int getId() {
		return keySet.getKeyAs(IntKey.class).getKey();
	}

	public boolean checkInsertScore(UUID player, double score) {
		if (score <= cacheWinThirdScore) {
			return false;
		}
		Map<UUID, Double> currentHighscore = new LinkedHashMap<>();

		if (cacheWinFirstId != null) {
			currentHighscore.put(cacheWinFirstId, cacheWinFirstScore);

			if (cacheWinSecondId != null) {
				currentHighscore.put(cacheWinSecondId, cacheWinSecondScore);

				if (cacheWinThirdId != null) {
					currentHighscore.put(cacheWinThirdId, cacheWinThirdScore);
				}
			}
		}
		currentHighscore.compute(player, (key, value) ->
				value != null ? Math.max(value, score) : score
		);

		List<Map.Entry<UUID, Double>> newHighscore = currentHighscore.entrySet()
				.stream()
				.sorted((elementA, elementB) -> elementB.getValue().compareTo(elementA.getValue()))
				.collect(Collectors.toList());

		int highscoreLength = newHighscore.size();
		if (highscoreLength >= 1) {
			this.cacheWinFirstId = newHighscore.get(0).getKey();
			this.cacheWinFirstScore = newHighscore.get(0).getValue();
			if (highscoreLength >= 2) {
				this.cacheWinSecondId = newHighscore.get(1).getKey();
				this.cacheWinSecondScore = newHighscore.get(1).getValue();
				if (highscoreLength >= 3) {
					this.cacheWinThirdId = newHighscore.get(2).getKey();
					this.cacheWinThirdScore = newHighscore.get(2).getValue();
				}
			}
		}
		return true;
	}

	@Override
	public KeySet getPrimaryKey() {
		return keySet;
	}
}
