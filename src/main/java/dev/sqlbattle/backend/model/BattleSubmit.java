package dev.sqlbattle.backend.model;

import dev.sqlbattle.backend.database.primarykey.IntKey;
import dev.sqlbattle.backend.database.primarykey.KeySet;

import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class BattleSubmit implements Model<BattleSubmit> {

	public KeySet keySet;
	@DatabaseName("battle_id")
	public int battleId;
	@DatabaseName("player_id")
	public UUID playerId;
	public String sql;
	@DatabaseName("preview_table")
	public String previewTable;
	public double score;

	public BattleSubmit(Integer key, int battleId, UUID playerId, String sql, String previewTable, double score) {
		this.keySet = new KeySet(new IntKey(key), "id");
		this.battleId = battleId;
		this.playerId = playerId;
		this.sql = sql;
		this.previewTable = previewTable;
		this.score = score;
	}

	@Override
	public KeySet getPrimaryKey() {
		return keySet;
	}
}
