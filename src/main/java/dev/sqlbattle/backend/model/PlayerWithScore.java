package dev.sqlbattle.backend.model;

import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class PlayerWithScore extends Player {

	@DatabaseIgnore
	public double score;

	public PlayerWithScore(UUID key, String username, String password, String image, String jobTitle, String website, String gitServer, String git, String twitter, String hyven, String codepen, double score) {
		super(key, username, password, image, jobTitle, website, gitServer, git, twitter, hyven, codepen);
		this.score = score;
	}
}
