package dev.sqlbattle.backend.general.security;

import java.util.UUID;
import java.util.function.BiFunction;

public interface HashFunction extends BiFunction<String, UUID, String> {

}
