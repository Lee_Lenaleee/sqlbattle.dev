package dev.sqlbattle.backend.general;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

@SuppressWarnings({ "deprecation", "MagicConstant" })
public class SafeResultSet implements ResultSet, AutoCloseable {

	private ResultSet internal;

	public SafeResultSet(ResultSet internal) {
		this.internal = internal;
	}

	@Override
	public boolean next() {
		try {
			return internal.next();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public void close() {
		try {
			internal.close();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public boolean wasNull() {
		try {
			return internal.wasNull();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public String getString(int i) {
		try {
			return internal.getString(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public boolean getBoolean(int i) {
		try {
			return internal.getBoolean(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public byte getByte(int i) {
		try {
			return internal.getByte(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public short getShort(int i) {
		try {
			return internal.getShort(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public int getInt(int i) {
		try {
			return internal.getInt(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public long getLong(int i) {
		try {
			return internal.getLong(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public float getFloat(int i) {
		try {
			return internal.getFloat(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public double getDouble(int i) {
		try {
			return internal.getDouble(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public BigDecimal getBigDecimal(int i, int i1) {
		try {
			return internal.getBigDecimal(i, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return new BigDecimal(-1);
	}

	@Override
	public byte[] getBytes(int i) {
		try {
			return internal.getBytes(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return new byte[0];
	}

	@Override
	public Date getDate(int i) {
		try {
			return internal.getDate(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Time getTime(int i) {
		try {
			return internal.getTime(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Timestamp getTimestamp(int i) {
		try {
			return internal.getTimestamp(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public InputStream getAsciiStream(int i) {
		try {
			return internal.getAsciiStream(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public InputStream getUnicodeStream(int i) {
		try {
			return internal.getUnicodeStream(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public InputStream getBinaryStream(int i) {
		try {
			return internal.getBinaryStream(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public String getString(String s) {
		try {
			return internal.getString(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public boolean getBoolean(String s) {
		try {
			return internal.getBoolean(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public byte getByte(String s) {
		try {
			return internal.getByte(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public short getShort(String s) {
		try {
			return internal.getShort(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public int getInt(String s) {
		try {
			return internal.getInt(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public long getLong(String s) {
		try {
			return internal.getLong(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public float getFloat(String s) {
		try {
			return internal.getFloat(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public double getDouble(String s) {
		try {
			return internal.getDouble(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public BigDecimal getBigDecimal(String s, int i) {
		try {
			return internal.getBigDecimal(s, i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return new BigDecimal(-1);
	}

	@Override
	public byte[] getBytes(String s) {
		try {
			return internal.getBytes(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return new byte[0];
	}

	@Override
	public Date getDate(String s) {
		try {
			return internal.getDate(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Time getTime(String s) {
		try {
			return internal.getTime(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Timestamp getTimestamp(String s) {
		try {
			return internal.getTimestamp(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public InputStream getAsciiStream(String s) {
		try {
			return internal.getAsciiStream(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public InputStream getUnicodeStream(String s) {
		try {
			return internal.getUnicodeStream(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public InputStream getBinaryStream(String s) {
		try {
			return internal.getBinaryStream(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public SQLWarning getWarnings() {
		try {
			return internal.getWarnings();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public void clearWarnings() {
		try {
			internal.clearWarnings();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public String getCursorName() {
		try {
			return internal.getCursorName();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public ResultSetMetaData getMetaData() {
		try {
			return internal.getMetaData();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Object getObject(int i) {
		try {
			return internal.getObject(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Object getObject(String s) {
		try {
			return internal.getObject(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public int findColumn(String s) {
		try {
			return internal.findColumn(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public Reader getCharacterStream(int i) {
		try {
			return internal.getCharacterStream(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Reader getCharacterStream(String s) {
		try {
			return internal.getCharacterStream(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(int i) {
		try {
			return internal.getBigDecimal(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(String s) {
		try {
			return internal.getBigDecimal(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public boolean isBeforeFirst() {
		try {
			return internal.isBeforeFirst();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean isAfterLast() {
		try {
			return internal.isAfterLast();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean isFirst() {
		try {
			return internal.isFirst();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean isLast() {
		try {
			return internal.isLast();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public void beforeFirst() {
		try {
			internal.beforeFirst();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void afterLast() {
		try {
			internal.afterLast();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public boolean first() {
		try {
			return internal.first();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean last() {
		try {
			return internal.last();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public int getRow() {
		try {
			return internal.getRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public boolean absolute(int i) {
		try {
			return internal.absolute(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean relative(int i) {
		try {
			return internal.relative(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean previous() {
		try {
			return internal.previous();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public void setFetchDirection(int i) {
		try {
			internal.setFetchDirection(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public int getFetchDirection() {
		try {
			return internal.getFetchDirection();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public void setFetchSize(int i) {
		try {
			internal.setFetchSize(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public int getFetchSize() {
		try {
			return internal.getFetchSize();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public int getType() {
		try {
			return internal.getType();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public int getConcurrency() {
		try {
			return internal.getConcurrency();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public boolean rowUpdated() {
		try {
			return internal.rowUpdated();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean rowInserted() {
		try {
			return internal.rowInserted();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public boolean rowDeleted() {
		try {
			return internal.rowDeleted();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public void updateNull(int i) {
		try {
			internal.updateNull(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBoolean(int i, boolean b) {
		try {
			internal.updateBoolean(i, b);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateByte(int i, byte b) {
		try {
			internal.updateByte(i, b);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateShort(int i, short i1) {
		try {
			internal.updateShort(i, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateInt(int i, int i1) {
		try {
			internal.updateInt(i, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateLong(int i, long l) {
		try {
			internal.updateLong(i, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateFloat(int i, float v) {
		try {
			internal.updateFloat(i, v);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateDouble(int i, double v) {
		try {
			internal.updateDouble(i, v);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBigDecimal(int i, BigDecimal bigDecimal) {
		try {
			internal.updateBigDecimal(i, bigDecimal);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateString(int i, String s) {
		try {
			internal.updateString(i, s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBytes(int i, byte[] bytes) {
		try {
			internal.updateBytes(i, bytes);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateDate(int i, Date date) {
		try {
			internal.updateDate(i, date);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateTime(int i, Time time) {
		try {
			internal.updateTime(i, time);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateTimestamp(int i, Timestamp timestamp) {
		try {
			internal.updateTimestamp(i, timestamp);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream, int i1) {
		try {
			internal.updateAsciiStream(i, inputStream, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream, int i1) {
		try {
			internal.updateBinaryStream(i, inputStream, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateCharacterStream(int i, Reader reader, int i1) {
		try {
			internal.updateCharacterStream(i, reader, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateObject(int i, Object o, int i1) {
		try {
			internal.updateObject(i, o, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateObject(int i, Object o) {
		try {
			internal.updateObject(i, o);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNull(String s) {
		try {
			internal.updateNull(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBoolean(String s, boolean b) {
		try {
			internal.updateBoolean(s, b);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateByte(String s, byte b) {
		try {
			internal.updateByte(s, b);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateShort(String s, short i1) {
		try {
			internal.updateShort(s, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateInt(String s, int i1) {
		try {
			internal.updateInt(s, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateLong(String s, long l) {
		try {
			internal.updateLong(s, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateFloat(String s, float v) {
		try {
			internal.updateFloat(s, v);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateDouble(String s, double v) {
		try {
			internal.updateDouble(s, v);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBigDecimal(String s, BigDecimal bigDecimal) {
		try {
			internal.updateBigDecimal(s, bigDecimal);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateString(String s, String s1) {
		try {
			internal.updateString(s, s1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBytes(String s, byte[] bytes) {
		try {
			internal.updateBytes(s, bytes);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateDate(String s, Date date) {
		try {
			internal.updateDate(s, date);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateTime(String s, Time time) {
		try {
			internal.updateTime(s, time);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateTimestamp(String s, Timestamp timestamp) {
		try {
			internal.updateTimestamp(s, timestamp);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream, int i1) {
		try {
			internal.updateAsciiStream(s, inputStream, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream, int i1) {
		try {
			internal.updateBinaryStream(s, inputStream, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateCharacterStream(String s, Reader reader, int i1) {
		try {
			internal.updateCharacterStream(s, reader, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateObject(String s, Object o, int i1) {
		try {
			internal.updateObject(s, o, i1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateObject(String s, Object o) {
		try {
			internal.updateObject(s, o);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void insertRow() {
		try {
			internal.insertRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateRow() {
		try {
			internal.updateRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void deleteRow() {
		try {
			internal.deleteRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void refreshRow() {
		try {
			internal.refreshRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void cancelRowUpdates() {
		try {
			internal.cancelRowUpdates();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void moveToInsertRow() {
		try {
			internal.moveToInsertRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void moveToCurrentRow() {
		try {
			internal.moveToCurrentRow();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public Statement getStatement() {
		try {
			return internal.getStatement();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Object getObject(int i, Map<String, Class<?>> map) {
		try {
			return internal.getObject(i, map);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Ref getRef(int i) {
		try {
			return internal.getRef(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Blob getBlob(int i) {
		try {
			return internal.getBlob(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Clob getClob(int i) {
		try {
			return internal.getClob(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Array getArray(int i) {
		try {
			return internal.getArray(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Object getObject(String s, Map<String, Class<?>> map) {
		try {
			return internal.getObject(s, map);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Ref getRef(String s) {
		try {
			return internal.getRef(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Blob getBlob(String s) {
		try {
			return internal.getBlob(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Clob getClob(String s) {
		try {
			return internal.getClob(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Array getArray(String s) {
		try {
			return internal.getArray(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Date getDate(int i, Calendar calendar) {
		try {
			return internal.getDate(i, calendar);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Date getDate(String s, Calendar calendar) {
		try {
			return internal.getDate(s, calendar);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Time getTime(int i, Calendar calendar) {
		try {
			return internal.getTime(i, calendar);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Time getTime(String s, Calendar calendar) {
		try {
			return internal.getTime(s, calendar);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Timestamp getTimestamp(int i, Calendar calendar) {
		try {
			return internal.getTimestamp(i, calendar);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Timestamp getTimestamp(String s, Calendar calendar) {
		try {
			return internal.getTimestamp(s, calendar);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public URL getURL(int i) {
		try {
			return internal.getURL(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public URL getURL(String s) {
		try {
			return internal.getURL(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public void updateRef(int i, Ref ref) {
		try {
			internal.updateRef(i, ref);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateRef(String s, Ref ref) {
		try {
			internal.updateRef(s, ref);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBlob(int i, Blob blob) {
		try {
			internal.updateBlob(i, blob);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBlob(String s, Blob blob) {
		try {
			internal.updateBlob(s, blob);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateClob(int i, Clob clob) {
		try {
			internal.updateClob(i, clob);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateClob(String s, Clob clob) {
		try {
			internal.updateClob(s, clob);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateArray(int i, Array array) {
		try {
			internal.updateArray(i, array);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateArray(String s, Array array) {
		try {
			internal.updateArray(s, array);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public RowId getRowId(int i) {
		try {
			return internal.getRowId(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public RowId getRowId(String s) {
		try {
			return internal.getRowId(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public void updateRowId(int i, RowId rowId) {
		try {
			internal.updateRowId(i, rowId);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateRowId(String s, RowId rowId) {
		try {
			internal.updateRowId(s, rowId);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public int getHoldability() {
		try {
			return internal.getHoldability();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return -1;
	}

	@Override
	public boolean isClosed() {
		try {
			return internal.isClosed();
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	@Override
	public void updateNString(int i, String s) {
		try {
			internal.updateNString(i, s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNString(String s, String s1) {
		try {
			internal.updateNString(s, s1);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNClob(int i, NClob nClob) {
		try {
			internal.updateNClob(i, nClob);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNClob(String s, NClob nClob) {
		try {
			internal.updateNClob(s, nClob);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public NClob getNClob(int i) {
		try {
			return internal.getNClob(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public NClob getNClob(String s) {
		try {
			return internal.getNClob(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public SQLXML getSQLXML(int i) {
		try {
			return internal.getSQLXML(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public SQLXML getSQLXML(String s) {
		try {
			return internal.getSQLXML(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public void updateSQLXML(int i, SQLXML sqlxml) {
		try {
			internal.updateSQLXML(i, sqlxml);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateSQLXML(String s, SQLXML sqlxml) {
		try {
			internal.updateSQLXML(s, sqlxml);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public String getNString(int i) {
		try {
			return internal.getNString(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public String getNString(String s) {
		try {
			return internal.getNString(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Reader getNCharacterStream(int i) {
		try {
			return internal.getNCharacterStream(i);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public Reader getNCharacterStream(String s) {
		try {
			return internal.getNCharacterStream(s);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public void updateNCharacterStream(int i, Reader reader, long l) {
		try {
			internal.updateNCharacterStream(i, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNCharacterStream(String s, Reader reader, long l) {
		try {
			internal.updateNCharacterStream(s, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream, long l) {
		try {
			internal.updateAsciiStream(i, inputStream, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream, long l) {
		try {
			internal.updateBinaryStream(i, inputStream, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateCharacterStream(int i, Reader reader, long l) {
		try {
			internal.updateCharacterStream(i, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream, long l) {
		try {
			internal.updateAsciiStream(s, inputStream, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream, long l) {
		try {
			internal.updateBinaryStream(s, inputStream, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateCharacterStream(String s, Reader reader, long l) {
		try {
			internal.updateCharacterStream(s, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBlob(int i, InputStream inputStream, long l) {
		try {
			internal.updateBlob(i, inputStream, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBlob(String s, InputStream inputStream, long l) {
		try {
			internal.updateBlob(s, inputStream, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateClob(int i, Reader reader, long l) {
		try {
			internal.updateClob(i, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateClob(String s, Reader reader, long l) {
		try {
			internal.updateClob(s, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNClob(int i, Reader reader, long l) {
		try {
			internal.updateNClob(i, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNClob(String s, Reader reader, long l) {
		try {
			internal.updateNClob(s, reader, l);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNCharacterStream(int i, Reader reader) {
		try {
			internal.updateNCharacterStream(i, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNCharacterStream(String s, Reader reader) {
		try {
			internal.updateNCharacterStream(s, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateAsciiStream(int i, InputStream inputStream) {
		try {
			internal.updateAsciiStream(i, inputStream);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBinaryStream(int i, InputStream inputStream) {
		try {
			internal.updateBinaryStream(i, inputStream);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateCharacterStream(int i, Reader reader) {
		try {
			internal.updateCharacterStream(i, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateAsciiStream(String s, InputStream inputStream) {
		try {
			internal.updateAsciiStream(s, inputStream);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBinaryStream(String s, InputStream inputStream) {
		try {
			internal.updateBinaryStream(s, inputStream);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateCharacterStream(String s, Reader reader) {
		try {
			internal.updateCharacterStream(s, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBlob(int i, InputStream inputStream) {
		try {
			internal.updateBlob(i, inputStream);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateBlob(String s, InputStream inputStream) {
		try {
			internal.updateBlob(s, inputStream);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateClob(int i, Reader reader) {
		try {
			internal.updateClob(i, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateClob(String s, Reader reader) {
		try {
			internal.updateClob(s, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNClob(int i, Reader reader) {
		try {
			internal.updateNClob(i, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public void updateNClob(String s, Reader reader) {
		try {
			internal.updateNClob(s, reader);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
	}

	@Override
	public <T> T getObject(int i, Class<T> aClass) {
		try {
			return internal.getObject(i, aClass);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public <T> T getObject(String s, Class<T> aClass) {
		try {
			return internal.getObject(s, aClass);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public <T> T unwrap(Class<T> aClass) {
		try {
			return internal.unwrap(aClass);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> aClass) {
		try {
			return internal.isWrapperFor(aClass);
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}
}
