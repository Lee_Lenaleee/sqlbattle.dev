package dev.sqlbattle.backend.general;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {

	public static int getYearsAgo(Date first, Date last) {
		Calendar a = Calendar.getInstance();
		a.setTime(first);
		Calendar b = Calendar.getInstance();
		b.setTime(last);
		int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
		if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH)) {
			diff--;
		}
		if (diff < 0) {
			if (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) < b.get(Calendar.DATE)) {
				diff++;
			}
		} else if (diff > 0) {
			if (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE)) {
				diff--;
			}
		}
		return diff;
	}

	public static int getYearsAgo(Date parse) {
		return getYearsAgo(parse, new Date());
	}
}
