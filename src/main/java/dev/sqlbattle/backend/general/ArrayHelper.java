package dev.sqlbattle.backend.general;

import org.apache.commons.lang3.ArrayUtils;

import java.util.function.IntFunction;
import java.util.function.Predicate;

public class ArrayHelper {

	public static <T> T[] combine(T[] firstPart, T[] secondPart, IntFunction<T[]> constructor) {
		T[] newArray = constructor.apply(firstPart.length + secondPart.length);

		System.arraycopy(firstPart, 0, newArray, 0, firstPart.length);
		System.arraycopy(secondPart, 0, newArray, firstPart.length, secondPart.length);

		return newArray;
	}

	public static <T> T[] splitArrayTill(T[] inputArray, Predicate<T> foundTill) {
		int i;
		for (i = 0; i < inputArray.length; i++) {
			if (foundTill.test(inputArray[i])) {
				break;
			}
		}
		return ArrayUtils.subarray(inputArray, 0, i);
	}
}
