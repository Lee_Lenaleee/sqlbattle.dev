package dev.sqlbattle.backend.general;

import java.util.function.Consumer;

public class RegexStringConsumer {

	public String regex;
	public Consumer<String> action;

	public RegexStringConsumer(String regex, Consumer<String> action) {
		this.regex = regex;
		this.action = action;
	}
}
