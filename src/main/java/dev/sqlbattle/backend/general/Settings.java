package dev.sqlbattle.backend.general;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@SuppressWarnings("unchecked")
public class Settings {

	public static final Settings SETTINGS = loadSettings();

	private static Settings loadSettings() {
		String file = "settings.json";
		try {
			return new Settings(new Gson().fromJson(new FileReader(file), Map.class));
		} catch (FileNotFoundException ex) {
			Logger.danger("File " + file + " not found!");
			throw new RuntimeException("File " + file + " not found!", ex);
		}
	}

	private Map<String, Object> settingsMap;

	private Settings(Map settings) {
		this.settingsMap = settings;
	}

	public int getInt(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof Number) {
			return ((Number) settingObject).intValue();
		} else {
			Logger.warn("Tried to get setting " + fullPath + " as int but failed casting");
			return -1;
		}
	}

	public double getDouble(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof Number) {
			return ((Number) settingObject).doubleValue();
		} else {
			Logger.warn("Tried to get setting " + fullPath + " as double but failed casting");
			return -1;
		}
	}

	public String getString(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof String) {
			String settingString = (String) settingObject;
			if (settingString.startsWith("$(") && settingString.endsWith(")")) {
				String path = settingString.substring(2, settingString.length() - 1);
				return SafeTry.execute(() ->
						new String(Files.readAllBytes(Paths.get(path)),
								StandardCharsets.UTF_8));
			}
			if (settingString.startsWith("|") && settingString.endsWith("|")) {
				return System.getenv(settingString.substring(1, settingString.length() - 1));
			}
			return settingString;
		} else if (settingObject instanceof Number || settingObject instanceof Boolean) {
			return settingObject.toString();
		} else {
			Logger.warn("Tried to get setting " + fullPath + " as string but failed casting");
			return settingObject == null ? "NULL" : settingObject.toString();
		}
	}

	public boolean getBoolean(String fullPath) {
		Object settingObject = get(fullPath);
		if (settingObject instanceof Boolean) {
			return (Boolean) settingObject;
		} else {
			Logger.warn("Tried to get setting " + fullPath + " as boolean but failed casting");
			return false;
		}
	}

	public Object get(String fullPath) {
		Map<?, ?> map = settingsMap;
		for (String path : fullPath.split("\\.")) {
			Object settingObject = map.get(path);
			if (settingObject instanceof Map) {
				map = (Map) settingObject;
				map.entrySet().forEach(settingSet -> {
					if (settingSet.getValue() instanceof Double && (Double) settingSet.getValue() % 1D == 0D) {
						((Map.Entry<Object, Number>) settingSet).setValue(((Double) settingSet.getValue()).intValue());
					}
				});
			} else {
				return settingObject;
			}
		}
		return map;
	}

	@Override
	public String toString() {
		return this.settingsMap.toString();
	}
}
