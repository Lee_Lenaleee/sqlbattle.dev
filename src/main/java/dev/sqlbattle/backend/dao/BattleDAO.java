package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.general.SafeResultSet;
import dev.sqlbattle.backend.model.Battle;
import dev.sqlbattle.backend.model.Player;

public class BattleDAO extends DatabaseAccessObject<Battle> {

	private BattleDAO() {
	}

	@Override
	public String getTableName() {
		return "battle";
	}

	@Override
	public Battle fromResultSet(SafeResultSet resultSet) {
		return new Battle(
				resultSet.getInt("id"),
				UUIDFromResultSet(resultSet, "submitted_by"),
				resultSet.getString("name"),
				resultSet.getString("desc"),
				resultSet.getString("image"),
				resultSet.getString("dataset"),
				resultSet.getString("preview_result"),
				resultSet.getString("test_dataset"),
				resultSet.getString("test_result"),
				UUIDFromResultSet(resultSet, "cache_win_1"),
				resultSet.getDouble("cache_win_1_score"),
				UUIDFromResultSet(resultSet, "cache_win_2"),
				resultSet.getDouble("cache_win_2_score"),
				UUIDFromResultSet(resultSet, "cache_win_3"),
				resultSet.getDouble("cache_win_3_score")
		);
	}
}
