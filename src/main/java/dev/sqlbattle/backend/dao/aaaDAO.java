package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.model.aaaModel;
import dev.sqlbattle.backend.general.SafeResultSet;

public class aaaDAO extends DatabaseAccessObject<aaaModel> {

	private aaaDAO() {
	}

	@Override
	public String getTableName() {
		return "aaa";
	}

	@Override
	public aaaModel fromResultSet(SafeResultSet resultSet) {
		return new aaaModel(
				resultSet.getString("")
		);
	}
}
