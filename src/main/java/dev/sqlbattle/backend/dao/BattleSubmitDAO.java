package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.general.SafeResultSet;
import dev.sqlbattle.backend.model.BattleSubmit;

public class BattleSubmitDAO extends DatabaseAccessObject<BattleSubmit> {

	private BattleSubmitDAO() {
	}

	@Override
	public String getTableName() {
		return "battle_submit";
	}

	@Override
	public BattleSubmit fromResultSet(SafeResultSet resultSet) {
		return new BattleSubmit(
				resultSet.getInt("id"),
				resultSet.getInt("battle_id"),
				UUIDFromResultSet(resultSet, "player_id"),
				resultSet.getString("sql"),
				resultSet.getString("preview_table"),
				resultSet.getDouble("score")
		);
	}
}
