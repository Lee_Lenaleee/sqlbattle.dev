function update(field, value, callback) {
    const data = {};
    data[field] = value ? value : $('#' + field).val();
    if (field === 'website') {
        data[field] = 'https://' + data[field];
    }
    const auth = localStorage.getItem('auth');

    $.ajax({
        url: '/user/update',
        method: 'POST',
        data,
        headers: {authorization: auth}
    }).done(function (updatedPlayer) {
        localStorage.setItem('self', JSON.stringify(updatedPlayer));
        if (callback) {
            callback();
        }
    });
}

function updateGit() {
    const gitServer = $('#git-select').val();
    const gitUsername = $('#git').val();

    if (gitUsername !== '') {
        update('git', 'https://' + gitServer + '.com/' + gitUsername);
    } else {
        update('git', '');
    }
}

function uploadImg(imgElement) {
    const canvas = document.getElementById('canvas-img').getContext('2d');
    const img = new Image(150, 150);
    img.src = URL.createObjectURL(imgElement.files[0]);
    img.onload = function () {
        canvas.drawImage(img, 0, 0, 150, 150);
    }
}

function updateImg() {
    const canvas = document.getElementById('canvas-img');
    update('image', canvas.toDataURL(), () => {
        location.reload(true);
    });
}

function logout() {
    localStorage.removeItem('self');
    localStorage.removeItem('auth');
    location.href = '/';
}

// Auto fillin current data
$(document).ready(function () {
    const self = JSON.parse(localStorage.getItem('self'));
    if (!self) {
        location.href = 'signup.html';
    }

    Object.keys(self).forEach(field => {
        if (field === 'image') return;
        if (field === 'website') self[field] = !self[field] ? self[field] : self[field].replace('https://', '');
        $('#' + field).val(self[field])
        if (field === 'gitServer') {
            let server = '';
            switch (self[field]) {
                case 'hub':
                    server = 'Github';
                    break;
                case 'lab':
                    server = 'Gitlab';
                    break;
                default:
                case 'bitbucket':
                    server = 'Bitbucket';
                    break;
            }
            $('#git-select').val(server);
        }
    })
});