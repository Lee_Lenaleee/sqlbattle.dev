(() => {
    const page = {
        id: new URL(location.href).searchParams.get('id'),
        timeout: 2500,
        allTablesSQL: "SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%' AND name NOT LIKE '__WebKitDatabaseInfoTable__';"
    };
    if (page.id == null) {
        location.href = '/';
        return;
    }

    if (typeof window.openDatabase !== 'function') {
        $(document).ready(function () {
            const anchor = $('.anchor.output h4, .anchor.output select');
            anchor.addClass('hidden');

            showError('Your browser does not support local testing of SQL', true)
            showError('You can still submit manualy or switch to an other browser', true)
        });
    } else {
        page.database = window.openDatabase('battle_1', '', 'battle', 1000 * 1000)
        page.databaseTest = window.openDatabase('battle_1_test', '', 'battle_test', 1000 * 1000)
    }

    function showSQLError(sql, error) {
        console.error(error);
        showError(`${sql.replaceAll('\n', ' ')}\n\n${error.split(/\(\d*/, 2)[1].replace(/\)$/, '')}`)
        return false;
    }

    function showError(text, includeConsole) {
        if (includeConsole) console.error(text);
        const anchor = $('.anchor.output');

        const errorDiv = $('<div class="alert alert-danger">')
        errorDiv.click(() => {
            errorDiv.remove();
        });
        errorDiv.text(text);
        errorDiv.html(errorDiv.html().replaceAll('\n', '<br>'));
        errorDiv.css('cursor', 'pointer');

        errorDiv.insertBefore(anchor.find('.anchor.table-result'));
    }

    function changeSQL(newSQL, forceNow) {
        const anchor = $('.anchor.chars');
        const currentScore = $('.current-score');
        anchor.text(newSQL.length);
        currentScore.text((Math.round(10000000 / newSQL.length) / 100).toLocaleString());

        if (page.pendingTest) {
            clearTimeout(page.pendingTest);
        }
        page.pendingTest = setTimeout(() => {
            localTest(newSQL);
        }, forceNow ? 25 : page.timeout);
    }

    function executeSQLFile(database, sql, callback) {
        database.transaction(transaction => {
            const statements = sql.split(';') // TODO: known bug when having ; inside comment
            statements.forEach((statement, index) => {
                if (statement.trim().length === 0 ||
                    statement.trim().startsWith('--')) {
                    return;
                }
                if (index !== statements.length - 1) {
                    statement = `${statement};`;
                }
                transaction.executeSql(statement, [], (transaction2, result) => {
                    if (callback) callback(statement, result.rows);
                }, (transaction, error) => {
                    return showSQLError(sql, error.message);
                });
            });
        });
    }

    function resultToArray(resultRows) {
        const array = [];
        for (let i = 0; i < resultRows.length; i++) {
            array[i] = resultRows[i];
        }
        return array;
    }

    function checkAnswer(resultRows, expectedResult, sql, isSecondTest) {
        const emoji = $('.target-result');
        const description = $('.target-result-text');
        const submitBtn = $('.btn-submit');

        if (resultRows == null) {
            emoji.text('❌');
            description.text('There was no result, so it cannot match either');
            return;
        }

        resultRows = resultToArray(resultRows);
        if (expectedResult !== JSON.stringify(resultRows)) {
            const reason = '';
            submitBtn.attr('disabled', 'disabled');
            if (isSecondTest) {
                emoji.text('✋');
                description.text('You almost got it, but it only works in this dataset and not always');
            } else {
                emoji.text('❌');
                description.text('The result does not match the target yet');
            }
            return;
        }
        if (isSecondTest) {
            // TODO: YOU GOT IT
            emoji.text('✔');
            if (localStorage.getItem('auth')) {
                submitBtn.removeAttr('disabled');
                description.text('You got it! You can now submit for a score');
            } else {
                description.text('You got it! Sign up / in and submit your score');
            }
            return;
        }

        dropAllTables(page.databaseTest);
        executeSQLFile(page.databaseTest, page.battle.testDataset);
        executeSQLFile(page.databaseTest, sql, (statement, rows) =>
            checkAnswer(rows, JSON.stringify(page.battle.testResult), sql, true));
    }

    function localTest(sql) {
        if (!page.database) {
            $('.btn-submit').removeAttr('disabled');
            return;
        }
        $('.anchor.output').find('.alert').remove();
        dropAllTables(page.database);
        executeSQLFile(page.database, page.battle.dataset);
        let executed = false;
        executeSQLFile(page.database, sql, (statement, rows) => {
            executed = true;
            showTableAsResult(statement, rows);
            checkAnswer(rows, JSON.stringify(page.battle.previewResult), sql, false);
        });
        if (!executed) {
            showTableAsResult(sql, []);
            checkAnswer();
        }
        showAllTables();
    }

    function showAllTables() {
        const dropdown = $('.db-dropdown');
        const loadingText = $('.db-loading-text');

        if (!page.database) {
            return;
        }
        page.database.transaction(transaction => {
            let hasTableSelected = false;

            transaction.executeSql(page.allTablesSQL, [], (transaction2, result) => {
                if (result.rows.length === 0) {
                    loadingText.text('No tables');
                } else {
                    dropdown.children().remove();
                    for (let i = 0; i < result.rows.length; i++) {
                        const tableName = result.rows[i].name;
                        const sql = `SELECT * FROM ${tableName} LIMIT 1;`;

                        loadingText.remove();
                        const option = $('<option>');
                        option.text(tableName);
                        option.addClass('item');
                        option.attr('disabled', '');
                        dropdown.append(option);

                        transaction.executeSql(sql, [], (transaction2, result) => {
                            if (result.rows.length > 0) {
                                if (!hasTableSelected) {
                                    hasTableSelected = true;
                                    option.attr('selected', 'selected');
                                    viewTable(tableName);
                                }
                                option.removeAttr('disabled');
                            }
                        }, (transaction, error) => {
                            return showSQLError(sql, error.message);
                        });
                    }
                }

                dropdown.change((event) => {
                    viewTable($(event.target).val());
                });
            }, (transaction, error) => {
                return showSQLError(sql, error.message);
            });
        });
    }

    function dropAllTables(database) {
        database.transaction(transaction => {
            transaction.executeSql(page.allTablesSQL, [], (transaction2, result) => {
                for (let i = 0; i < result.rows.length; i++) {
                    const sql = `DROP TABLE ${result.rows[i].name};`;
                    transaction.executeSql(sql, [], (transaction2, result) => {
                    }, (transaction, error) => {
                        return showSQLError(sql, error.message);
                    });
                }
            }, (transaction, error) => {
                return showSQLError(page.allTablesSQL, error.message);
            });
        });
    }

    function viewTable(tableName) {
        const anchor = $('.anchor.table-view');
        if (!page.database) {
            return;
        }
        page.database.transaction(transaction => {
            const sql = `SELECT * FROM ${tableName} LIMIT 1000;`;
            transaction.executeSql(sql, [], (transaction2, result) => {
                const table = datasetToTable(sql, result.rows);
                anchor.html('');
                anchor.append(table);
            }, (transaction, error) => {
                return showSQLError(sql, error.message);
            });
        }, (transaction, error) => {
            return showSQLError(sql, error.message);
        });
    }

    function showTableAsResult(statement, sqlResultSetRows) {
        const anchor = $('.anchor.table-result');
        const table = datasetToTable(statement, sqlResultSetRows)

        anchor.html('');
        anchor.append(table);
    }

    function targetAsTable(sqlResultSetRows) {
        const anchor = $('.anchor.table-target');
        const table = datasetToTable('', sqlResultSetRows)

        anchor.html('');
        anchor.append(table);
    }

    function datasetToTable(statement, sqlResultSetRows) {
        let dataHeaders = ['No results']
        if (sqlResultSetRows.length > 0) {
            dataHeaders = Object.keys(sqlResultSetRows[0]);
        }
        const table = $('<table class="table result-data table-sm table-striped">');
        table.attr('title', statement);
        const tableHead = $('<thead>');
        const tableHeadRow = $('<tr>');
        if (sqlResultSetRows.length > 0) {
            const tableHeadColumnId = $('<th>');
            tableHeadColumnId.text('rowid');
            tableHeadRow.append(tableHeadColumnId);
        }
        dataHeaders.forEach(columnName => {
            const tableHeadColumn = $('<th>');
            tableHeadColumn.text(columnName);
            tableHeadRow.append(tableHeadColumn);
        });
        tableHead.append(tableHeadRow);
        table.append(tableHead);

        const tableBody = $('<tbody>');
        for (let i = 0; i < sqlResultSetRows.length; i++) {
            const tableBodyRow = $('<tr>');
            const row = sqlResultSetRows[i];

            const tableBodyDataId = $('<th>');
            tableBodyDataId.text(`#${i + 1}`);
            tableBodyRow.append(tableBodyDataId)
            dataHeaders.forEach(header => {
                const tableBodyData = $('<td>');
                tableBodyData.text(row[header]);
                tableBodyRow.append(tableBodyData)
            });
            tableBody.append(tableBodyRow);
        }
        table.append(tableBody);

        return table;
    }

    function afterLoadBattle(page) {
        let value = 'SELECT 1;\n-- Write your SQL code here, you will see the results happen next to them\n-- and next to the results are the expected results';

        const storageValue = localStorage.getItem(`battle_${page.id}`);
        if (storageValue) {
            value = storageValue;
        }

        changeSQL(value, true);

        const {EditorState, basicSetup} = CM["@codemirror/basic-setup"];
        const {EditorView, keymap} = CM["@codemirror/view"];
        const {history, historyKeymap} = CM["@codemirror/history"];
        const {defaultKeymap} = CM["@codemirror/commands"];
        const {sql} = CM["@codemirror/lang-sql"];

        const editor = new EditorView({
            state: EditorState.create({
                doc: value,
                extensions: [basicSetup, EditorView.lineWrapping, history(), keymap.of([...defaultKeymap, ...historyKeymap]), sql()],
                tabSize: 4,
                lineWrapping: true,
            }), parent: document.querySelector("#editor")
        });

        let lastValue = "";
        setInterval(() => {
            const value = editor.state.doc.toString();
            if (lastValue === value) {
                return;
            }
            lastValue = value;
            changeSQL(value)
            localStorage.setItem(`battle_${page.id}`, value);
        }, 500);
        $('.btn-test').click(() => {
            const value = editor.state.doc.toString();
            changeSQL(value, true);
        });

        const auth = localStorage.getItem('auth');
        $('.btn-submit').click(() => {
            $.ajax({
                url: `/submit/${page.id}`,
                method: 'PUT',
                headers: {authorization: auth},
                data: {sql: editor.state.doc.toString()}
            }).done(function (submitResult) {
                console.log(submitResult);
                if (isResponseError(submitResult)) {
                    console.error(submitResult);
                    return;
                }
                if (submitResult) {
                    localStorage.setItem('self', JSON.stringify(submitResult));
                }
            });
        });
    }

    // Title and Description of battle
    $(document).ready(function () {
        const anchorTitle = $('.anchor.battle-title');
        const parentTitle = anchorTitle.parent();
        const anchorDesc = $('.anchor.battle-desc');
        const parentDesc = anchorDesc.parent();

        $.getJSON('/battle/' + page.id, battle => {
            if (isResponseError(battle)) {
                parentTitle.text('ERROR');
                parentDesc.text('Battle with id ' + page.id + ' not found');
                console.error(battle);
                return;
            }
            page.battle = battle;

            parentTitle.text(battle.name);
            parentDesc.html(battle.desc);
            battle.previewResult = JSON.parse(battle.previewResult);
            battle.testResult = JSON.parse(battle.testResult);
            targetAsTable(battle.previewResult);

            afterLoadBattle(page);
        });
    });
})();
