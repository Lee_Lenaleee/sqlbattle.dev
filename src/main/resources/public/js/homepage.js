function buildImageTopScore(user, score) {
    const imageElement = $('<img>');
    imageElement.attr('alt', user.username);
    imageElement.attr('data-bs-toggle', 'popover');
    imageElement.attr('data-bs-placement', 'bottom');
    imageElement.attr('data-bs-trigger', 'focus');
    imageElement.attr('data-bs-title', user.username);
    imageElement.attr('data-bs-content', `Points: ${score.toFixed(0)}`);
    imageElement.attr('data-bs-container', 'body');
    imageElement.attr('src', user.image ? user.image : 'img/placeholder.png');
    imageElement.attr('data-popover-todo', 'true');

    // console.log(new bootstrap.Popover(imageElement[0])); // TODO: doesnt work?
    imageElement.attr('title', user.username + '\nPoints: ' + score.toFixed(0));

    return imageElement;
}

function buildChallengeBlock(battle) {
    // <div class="challenge">
    //     <h3 class="name">{{ name }}</h3>
    //     <div>
    //         <p>{{ desc }}</p>
    //         <div class="leaders">
    //             <h4>Top 3:</h4>
    //             <img alt="{{ name }}" src="img/dataset.png">
    //             <img alt="{{ name }}" src="img/dataset.png">
    //             <img alt="{{ name }}" src="img/dataset.png">
    //         </div>
    //     </div>
    //     <img alt="{{ name }}" src="img/dataset.png">
    //     <span>{{ number }}</span>
    // </div>

    const challengeDiv = $('<div class="challenge">');
    const challengeTitle = $('<h3 class="name">');
    challengeTitle.text(battle.name)
    challengeTitle.click(() => window.location.href = `/battle.html?id=${battle.id}`);
    const leftDiv = $('<div>');
    const challengeDesc = $('<article>');
    challengeDesc.html(battle.desc);
    const top3Div = $('<div class="leaders">');
    const top3Title = $('<h4>');
    if (battle.cacheWinFirst) {
        top3Title.text('Top 1:');
        top3Div.append(buildImageTopScore(battle.cacheWinFirst, battle.cacheWinFirstScore));

        if (battle.cacheWinSecond) {
            top3Title.text('Top 2:');
            top3Div.append(buildImageTopScore(battle.cacheWinSecond, battle.cacheWinSecondScore));

            if (battle.cacheWinThird) {
                top3Title.text('Top 3:');
                top3Div.append(buildImageTopScore(battle.cacheWinThird, battle.cacheWinThirdScore));
            }
        }

        top3Div.prepend(top3Title);
    }

    const challengeImage = $('<img>');
    challengeImage.attr('alt', battle.name);
    challengeImage.attr('src', battle.image.trim() ? battle.image : '/img/placeholder.png');
    challengeImage.click(() => window.location.href = `/battle.html?id=${battle.id}`);
    const challengeNumber = $('<span>');
    challengeNumber.text(`#${battle.id}`);
    challengeNumber.click(() => window.location.href = `/battle.html?id=${battle.id}`);

    challengeDiv.append(challengeTitle);
    challengeDiv.append(leftDiv)
    leftDiv.append(challengeDesc)
    leftDiv.append(top3Div);

    challengeDiv.append(challengeImage)
    challengeDiv.append(challengeNumber)
    return challengeDiv;
}

// Challenges on site Latest battles
$(document).ready(function () {
    const anchor = $('.anchor.challenges-latest');
    const parent = anchor.parent();

    $.getJSON('/battles', function (battles) {
        if (isResponseError(battles)) {
            anchor.text('ERROR');
            console.error(battles);
            return;
        }
        anchor.remove();

        battles.forEach(battle => {
            const challengeDiv = buildChallengeBlock(battle)
            parent.append(challengeDiv);
        });
    });
});

// Challenges on site Latest battles
$(document).ready(function () {
    const anchor = $('.anchor.challenges-newfirst');
    const parent = anchor.parent();

    $.getJSON('/battles-new', function (battles) {
        if (isResponseError(battles)) {
            anchor.text('ERROR');
            console.error(battles);
            return;
        }
        anchor.remove();

        battles.forEach(battle => {
            const challengeDiv = buildChallengeBlock(battle)
            parent.append(challengeDiv);
        });
    });
});

