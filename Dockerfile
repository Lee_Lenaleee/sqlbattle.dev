FROM docker.io/gradle:jdk8

COPY . .
RUN gradle shadow
RUN mv "./build/libs/SQLBattle-23.4-SNAPSHOT-all.jar" .
CMD java -jar "SQLBattle-23.4-SNAPSHOT-all.jar"
