# SQLBattle
SQLBattle.dev is a site where you can show your SQL skills on a conceptual level.  
You will get more points when you use SQL with at as few characters as possible.  

## Architecture 
Plain HTML, CSS, JS is the frontend.  
This is subject to change, maybe JTwig will be used in the future.  

Backend is JavaSpark (not from Apache) with a custom-made framework for faster backend development.
So no help online for how it works, but its very readable how it works.

## How to work on SQLBattle
### Clone and import it into IDE
**First clone by using**  
``git clone https://gitlab.com/MakerTim/sqlbattle.dev.git SQLBattle``  
Then import the project by importing the build.gradle  

**IntelliJ**  
``Open project > browse to the SQLBattle folder > select the build.gradle file``  
https://www.jetbrains.com/help/idea/gradle.html#gradle_import_project_start  
**Eclipse**  
Run ``gradle eclipse`` and open the folder  
https://www.eclipse.org/community/eclipse_newsletter/2018/february/buildship.php  
**Visual studio code**  
https://code.visualstudio.com/docs/java/java-build#_gradle

### Local development
Inside ``settings.json`` are some environment variables  
Most of them will work for you default  
**MUST CHANGE** the database credentials  
In default configuration the server will start on ip ``0.0.0.0`` at port ``8181``  

Easy way of changing the database configuration is by using the environment variables
![alt text](https://i.imgur.com/M6yZfG0.png)

**Database**  
The database name is inside the jdbc inside the settings, defaults its ``sqlbattle``.  
There is an empty database structure file for migration ``db.sql`` inside the root directory.

### Run server
Compile the Java code ``gradle shadowJar``  
And run the jar by ``java -jar build/libs/SQLBattle-23.4-SNAPSHOT-all.jar``  
**Smart IDE's** (IntelliJ)  
If your IDE supports it, you can go to the main class at src/main/java/``dev.sqlbattle.backend.Main``
an from there you can run the main method.  
For frontend development, run jar as debug mode and "build" the jar for every change you make. This change should be live afterwards.

**Note**
Spark.java does not allow for live reloading, use your IDE its function to debug and re-build the project for faster development on the frontend
